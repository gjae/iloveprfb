export default {
    darkPrimaryColor: "#D32F2F",
    defaultPrimaryColor: "#F44336",
    lightPrimaryColor: "#FFCDD2",
    textPrimaryColor: "#FFFFFF",
    accentColor: "#FF5252",
    primaryTextColor: "#212121",
    secondaryTextColor: "#757575",
    dividerColor: "#BDBDBD",
    firstFontFamily: "roboto" ,
    secondaryFontFamily: "roboto"  
}