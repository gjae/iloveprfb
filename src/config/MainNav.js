import React from 'react';
import {createAppContainer, createStackNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import LoginScreen from '../components/screen/LoginScreen'
import RegisterScreen from '../components/screen/RegisterScreen'
import LoginFormScreen from '../components/screen/LoginFormScreen'
import ForgotPassword from '../components/screen/ForgotPassword'
import HomeScreen from '../components/screen/HomeScreen'
import MapScreen from '../components/screen/MapScreen'
import colors from '../assets/pallete.js';

const tabNav = createMaterialTopTabNavigator({

    home: {
        screen: HomeScreen,
        navigationOptions: {
            title: "Inicio"
        }
    },
    cerca: {
        screen: MapScreen,
        navigationOptions: {
            title: "Cerca"
        }
    }

}, {
    tabBarOptions : {
        style: {
            backgroundColor: colors.defaultPrimaryColor,
        },
        labelStyle: {
            fontSize: 18,
            fontFamily: colors.fontFamily
        },
        indicatorStyle: {
            backgroundColor: "#ffffff"
        },
        upperCaseLabel: false
    },
    lazy: true
})

const baseNav = createStackNavigator({
    loginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            header: ()=> null
        }
    },
    register: {
        screen: RegisterScreen
    },
    loginForm: {
        screen: LoginFormScreen
    },
    forgotPassword: {
        screen: ForgotPassword
    },
    user: {
        screen: tabNav,
        navigationOptions: {
            title: "I love PR",
            headerStyle: {
                backgroundColor: colors.defaultPrimaryColor,
                elevation: 0
            },
            headerTintColor: colors.textPrimaryColor,
            headerTitleContainerStyle: {
                width: "68%",
                justifyContent: "center",
                alignItems: "center",
                alignContent: "center"
            },
            headerTitleStyle: {
                fontFamily: colors.firstFontFamily
            },
            headerLeft: () => null
        }
    }
})


export default createAppContainer(baseNav);