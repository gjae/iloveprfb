import React from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    TouchableHighlight
} from 'react-native';

import colors from '../../assets/pallete.js';

const AppButton = (props) => (
    <TouchableHighlight {...props} style={[styles.frame, props.frameStyle]}>
        <View style={ styles.containerStyle }>
            {props.image}
            <Text style={ styles.textStyles }> 
                {props.textButton} 
            </Text>
        </View>
    </TouchableHighlight>
)

export default AppButton;

const styles = StyleSheet.create({
    frame: {
        width: 77,
        height: 65,
        backgroundColor: "#f3f3f3",
        borderWidth: 1,
        borderColor: "#f3f3f3",
        alignContent: "center",
        alignItems: "center"
    },
    textStyles: { 
        textAlign: "center", 
        color: colors.secondaryTextColor, 
        fontSize: 15 
    },
    containerStyle: { 
        flex: 1, 
        flexDirection: "column", 
        alignItems: "center", 
        alignContent: "center" ,
        paddingTop: 10
    }
})