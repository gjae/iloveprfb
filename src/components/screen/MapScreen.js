import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import {
    Input,
    Divider
} from 'react-native-elements';
import colors from '../../assets/pallete.js';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';

const { width, height } = Dimensions.get('window');
export default class MapScreen extends React.Component {
    
    constructor(props){
        super(props);
        
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <View style={styles.mainContentStyle}>
                    <View style={{ paddingLeft: 10, flex: .1 ,flexDirection: "row", width: "100%", height: 50, borderBottomWidth: 1, borderBottomColor: colors.secondaryTextColor  }}>
                        <TouchableOpacity style={{ borderRightWidth: 1, borderRightColor: colors.secondaryTextColor, paddingRight: 10 , alignItems: "flex-start", borderBottomWidth: 1, borderBottomColor: "#f3f3f3", paddingTop: 10 }}>
                            <View>
                                <Text style={{ color: colors.secondaryTextColor, fontSize: 24 }}>
                                   <Icon name="md-pin" color={colors.secondaryTextColor} size={24} /> Ciudad
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{  alignItems: "flex-start", borderBottomWidth: 1, borderBottomColor: "#f3f3f3", paddingTop: 10, marginLeft: 10}}>
                            <View>
                                <Text style={{ color: colors.secondaryTextColor, fontSize: 24 }}>
                                   <Icon name="md-map" color={colors.secondaryTextColor} size={24} /> Ver en mapa
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Divider />
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        width,
        height,
        backgroundColor: "#FFFFFF"
    },
    mainContentStyle: {
        width,
        height,
        paddingTop: 1
    },
    inputContainerStyle: {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0,
        height: 60
    },
    inputContentStyle:  {
        backgroundColor: "#ffffff",
        marginLeft: 0,
        width: "50%",
        height: 60
    }
})