import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    StatusBar,
    Image,
    TouchableHighlight,
    Alert
} from 'react-native';

import {
    Input,
    Divider
} from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';

import { AccessToken, LoginManager } from 'react-native-fbsdk';

import colors from '../../assets/pallete.js';
import firebase from 'react-native-firebase';

const { height, width } = Dimensions.get('window');

export default class LoginScreen extends React.Component {

    constructor(props){
        super(props);

        this._onFacebookButtonPress = this._onFacebookButtonPress.bind(this);
    }

    _onFacebookButtonPress = async() =>{
        try{
            const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);

            // VERIFICAR SI NO HA SIDO CANCELADO EL LOGIN
            if (result.isCancelled) {
              throw new Error('User cancelled request'); // Handle this however fits the flow of your app
            }

            const data = await AccessToken.getCurrentAccessToken();

            if (!data) {
              throw new Error('Something went wrong obtaining the users access token'); // Handle this however fits the flow of your app
            }
        

            // create a new firebase credential with the token
            const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

            // login with credential
            const currentUser = await firebase.auth().signInWithCredential(credential);

            this.props.navigation.nvigate('user');

        }catch(e){
            // Alert.alert("--", JSON.stringify(e))
        }
    }

    // async componentWillMount() {
    //     let user = firebase.auth().currentUser
    //     if( user )
    //         this.props.navigation.navigate('user');
    // }

    render(){
        return(
            <View style={styles.mainContainer}>
                <StatusBar backgroundColor={colors.darkPrimaryColor} />
                <View style={styles.topContent}>
                    <View style={{ backgroundColor: colors.defaultPrimaryColor, height: "20%" }} />
                    <View style={{ width, padding: 10 }}>
                        <Text style={[styles.textStyles, {fontSize: 22, textAlign: "center"}]}>
                            Si te registras tendrás estos beneficios.
                        </Text>
                    </View>
                    {/** AREA DE LOS ICONOS DEL LOGIN */}
                    <View style={{ width, flex: 1, flexDirection: "column", padding: 10 }}>
                        <View style={{ flex: 1, flexDirection: "row", padding: 4 }}>
                            <Image style={styles.pngIconsStyle} source={require('../../assets/png_icons/bookmark.png')} />
                            <Text style={[styles.secondaryText]} >
                                Puedes guardar tus lugares favoritos y visitados
                            </Text>
                        </View>
                    </View>
                    <View style={{ width, flex: 1, flexDirection: "column", padding: 10, marginTop: "-19%"}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <Image style={styles.pngIconsStyle} source={require('../../assets/png_icons/star.png')} />
                            <Text style={[styles.secondaryText]} >
                                Acumulas puntos por escribir resemas, subir fotos, reservar online, etc.
                            </Text>
                        </View>
                    </View>
                    <View style={{ width, flex: 1, flexDirection: "column", padding: 10, marginTop: "-19%" }}>
                        <View style={{ flex: 1, flexDirection: "row"}}>
                            <Image style={styles.pngIconsStyle} source={require('../../assets/png_icons/price.png')} />
                            
                            <Text style={[styles.secondaryText]} >
                                Tendrás acceso a <Text style={[styles.textStyles, {fontSize: 18, fontWeight: "bold"}]}>descuentos exclusivos </Text>
                                {'\n'} y control sobre las notificaciones que {'\n'}enviamos.
                            </Text>
                        </View>
                    </View>
                    <View style={{ marginTop: "-18%" }}>
                        <Divider backgroundColor={colors.dividerColor} />
                    </View>
                    <View style={{ width, flex: 1, flexDirection: "column", padding: 10, alignItems: "center"}}>

                        <TouchableHighlight onPress={()=> this._onFacebookButtonPress() } style={{ width: "95%", height: 50, backgroundColor: "#204A87", borderRadius: 4 }} >
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ borderRightWidth: .5, borderRightColor: colors.secondaryTextColor , width: "15%", alignContent: "center", alignItems: "center", justifyContent: "center" }}>
                                    <Text style={{ color: "#ffffff", fontSize: 24, fontWeight: "bold", textAlign: "center" }}>
                                        f
                                    </Text>
                                </View>
                                <View style={{ alignContent: "center", alignItems: "center", justifyContent: "center", paddingLeft: 10 }} >
                                    <Text style={{ color: "#ffffff", fontSize: 24, fontWeight: "bold" }}>
                                        Entrar con facebook
                                    </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                        <Text style={{ color: colors.secondaryTextColor, fontFamily: colors.secondaryFontFamily, fontSize: 18, textAlign: "center" }}>
                            Nunca publicaremos algo sin preguntarte
                        </Text>
                        <View style={{ flex: 1, flexDirection: "row", width, border: 1, borderColor: colors.secondaryTextColor, justifyContent: "space-between", alignItems: "center", alignContent: "center" }}>
                            <TouchableHighlight onPress={()=> this.props.navigation.navigate('loginForm') }  style={{ width: "50%", alignContent: "center", alignItems: "center", borderWidth: 1, borderColor: colors.secondaryTextColor, height: "100%", alignItems: "center", alignContent: "center", paddingTop: 10 }}>
                                <Text style={{ textAlign: "center", fontSize: 22 , fontWeight: "bold", color: colors.primaryTextColor }} >
                                    Inicia sesión
                                </Text>
                            </TouchableHighlight>
                            <TouchableHighlight onPress={()=> this.props.navigation.navigate('register')} style={{ width: "50%", alignContent: "center", alignItems: "center", borderWidth: 1, borderColor: colors.secondaryTextColor, height: "100%", alignItems: "center", alignContent: "center", paddingTop: 10 }}>
                                <Text style={{ textAlign: "center", fontSize: 22 , fontWeight: "bold", color: colors.primaryTextColor }} >
                                    Regístrar
                                </Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    {/** FIN DEL AREA DE LOS ICONOS */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        height,
        width
    },
    topContent: {
        flex: 1,
        flexDirection: "column",
        height,
        width
    },
    textStyles: {
        fontFamily: colors.firstFontFamily,
        color: colors.primaryTextColor
    },
    secondaryText: {
        fontFamily: colors.secondaryFontFamily,
        color: colors.secondaryTextColor,
        fontSize: 18,
        marginLeft: 9
    },
    pngIconsStyle: {
        width: 34,
        height: 34
    }
})