import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    FlatList,
    ScrollView,
    Dimensions,
    Alert,
    Image,
    TouchableHighlight
} from 'react-native';

import {
    Tile,
    Card
} from 'react-native-elements';

import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../assets/pallete.js';
import AppButton from '../ui/AppButton';

const { width, height } = Dimensions.get('window');
const lateralPadding = 15;
export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lugares: [],
            populares: [],
            recomendaciones: []
        }
    }

    addToArray(data){
        let { lugares } = this.state
        this.setState({ lugares: lugares.concat(data) })
    }

    addToPopulars(data){
        let { populares } = this.state
        this.setState({ populares: populares.concat(data) })
    }

    addToRecomendaciones(data) {
        let { recomendaciones } = this.state
        this.setState({ recomendaciones: recomendaciones.concat(data) })

    }

    componentDidMount(){

        firebase.firestore()
        .collection('lugares')
        .get()
        .then( snapShot =>{
            snapShot.forEach( doc =>{
                doc.data().ciudad.get()
                .then(  async(resp) =>{
                    let { lugares } = this.state;
                    let newArray = lugares.concat({ 
                        id: lugares.length,
                        lugar: doc.data(), 
                        ciudad: resp.data(),
                        categoria: await doc.data().categoria.get().then( doc => doc.data() )
                    })
                    this.addToArray(newArray)
                })
            })
        } )

        firebase.firestore()
        .collection('lugares')
        .get()
        .then( snapShot =>{
            snapShot.forEach( doc =>{
                doc.data().ciudad.get()
                .then(  async(resp) =>{
                    let { populares } = this.state;
                    let newArray = populares.concat({ 
                        id: populares.length,
                        lugar: doc.data(), 
                        ciudad: resp.data(),
                        categoria: await doc.data().categoria.get().then( doc => doc.data() )
                    })
                    this.addToPopulars(newArray);
                })
            })
        } )

        firebase.firestore()
        .collection('lugares')
        .get()
        .then( snapShot =>{
            snapShot.forEach( doc =>{
                doc.data().ciudad.get()
                .then(  async(resp) =>{
                    let { recomendaciones } = this.state;
                    let newArray = recomendaciones.concat({ 
                        id: recomendaciones.length,
                        lugar: doc.data(), 
                        ciudad: resp.data(),
                        categoria: await doc.data().categoria.get().then( doc => doc.data() )
                    })
                    this.addToRecomendaciones(newArray);
                })
            })
        } )

    }

    render() {
        return(
            <ScrollView style={styles.mainContainer} >
                <FlatList
                    data={this.state.lugares}
                    keyExtractor={ (item, id) => item.id}
                    horizontal={true}
                    renderItem={ ({item})=> (
                        <Tile 
                            key={item.id}
                            imageSrc={{ uri: item.lugar.imagenes[0] }}
                            titleStyle={{ color: "#ffffff", fontFamily: "roboto" }}
                            height={ width * 0.4 }
                            width={ width * 0.92 }
                            containerStyle={{ marginRight: 10 }}
                            contentContainerStyle={{
                                position: "absolute",
                                backgroundColor: "transparent",
                                bottom: 0,
                                left: 0,
                                right: 0
                            }}
                        >
                            <View style={{ width: "100%" }} >
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "space-between" }}>
                                    <View style={{ flex: 1, flexDirection: "column", justifyContent: "space-between" }}>
                                        <Text style={{ color :"#ffffff", fontSize: 24, fontFamily: "roboto" }} >
                                            {item.lugar.nombre_lugar}
                                        </Text>
                                        <Text style={{ color :"#ffffff", fontSize: 17, fontFamily: "roboto" }} >
                                            <Icon name="md-pin" color="#ffffff" size={20} /> {item.ciudad.nombre}
                                        </Text>
                                    </View>
                                    <View style={{ alignContent: "center", alignItems: "center"}}>
                                        <View style={styles.tileBadgeStyle}> 
                                            <Text style={{ color: "#ffffff", fontSize: 13, fontFamily: "roboto", textAlign: "center" }}>
                                                {item.categoria.nombre_categoria}
                                            </Text>
                                            <Text style={{ color: "#ffffff", fontSize: 19.5, fontFamily: "roboto", fontWeight: "bold", textAlign: "center" }}>
                                                {item.lugar.ranking}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </Tile>
                    )}
                />
                <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text style={{ fontSize: 24, color: colors.primaryTextColor, fontFamily: colors.fontFamily, marginTop: 4 }} >
                        Explora
                    </Text>
                    <View style={{ flex: 1, flexDirection: "column", marginTop: 10 }}>
                        <View style={{ flex: 1, flexDirection: "row"}} >
                            <AppButton 
                                textButton={"Zona"} 
                                image={<Image source={require('../../assets/png_icons/map_marker.png')} style={{ width: 30, height: 30 }} />} 
                            />
                            <AppButton 
                                textButton={"Ranking"} 
                                image={<Image source={require('../../assets/png_icons/ranking.png')} style={{ width: 30, height: 30 }} />} 
                                frameStyle={{ marginLeft: 8 }}
                            />
                            <AppButton 
                                textButton={"Descuento"} 
                                image={<Image source={require('../../assets/png_icons/discount.png')} style={{ width: 30, height: 30 }} />} 
                                frameStyle={{ marginLeft: 8 }}
                            />
                            <AppButton 
                                textButton={"Eventos"} 
                                image={<Image source={require('../../assets/png_icons/event.png')} style={{ width: 30, height: 30 }} />} 
                                frameStyle={{ marginLeft: 8 }}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: "row", marginTop: 5}} >
                            <AppButton 
                                textButton={"Restaurante"} 
                                image={<Image source={require('../../assets/png_icons/food.png')} style={{ width: 30, height: 30 }} />} 
                            />
                            <AppButton 
                                textButton={"Activities"} 
                                image={<Image source={require('../../assets/png_icons/exit-door.png')} style={{ width: 30, height: 30 }} />} 
                                frameStyle={{ marginLeft: 8 }}
                            />
                            <AppButton 
                                textButton={"Hoteles"} 
                                image={<Image source={require('../../assets/png_icons/sleep.png')} style={{ width: 30, height: 30 }} />} 
                                frameStyle={{ marginLeft: 8 }}
                            />
                            <AppButton 
                                textButton={"Ver todos"} 
                                image={<Image source={require('../../assets/png_icons/24-hours.png')} style={{ width: 30, height: 30 }} />} 
                                frameStyle={{ marginLeft: 8 }}
                           />
                        </View>
                    </View>
                    
                    <Text style={{ fontSize: 24, color: colors.primaryTextColor, fontFamily: colors.fontFamily, marginTop: 4 }} >
                        Populares
                    </Text>
                    <FlatList
                        data={this.state.populares}
                        keyExtractor={ (item, id) => item.id}
                        horizontal={true}
                        renderItem={({item})=>(
                            <TouchableHighlight>
                                <Card
                                    containerStyle={{
                                        width: width * 0.6,
                                        padding:0,
                                        marginLeft: 0,
                                        marginTop: 2
                                    }}
                                    image={{ uri: item.lugar.imagenes[0] }}>
                                    <View style={{ alignItems: "center",alignContent: "center", paddingTop: 5 ,position: "absolute", width: 45, height: 35, backgroundColor: colors.accentColor, top: "-290%", left: "86%"}}>
                                        <Text style={{color: "#ffffff", textAlign: "center", textAlignVertical: "center", fontSize: 20}}>
                                            {item.lugar.ranking}
                                        </Text>
                                    </View>
                                    <Text style={{color: colors.primaryTextColor, fontSize: 19}}>
                                        {item.lugar.nombre_lugar}
                                    </Text>
                                    <Text style={{marginBottom: 10, color: colors.secondaryTextColor, fontSize: 14}}>
                                        <Icon name="md-pin" color={colors.primaryTextColor} size={14} /> {item.ciudad.nombre}
                                    </Text>
                                </Card>
                            </TouchableHighlight>
                        )}
                    />

                    <Text style={{ fontSize: 24, color: colors.primaryTextColor, fontFamily: colors.fontFamily, marginTop: 4 }} >
                        Recomendaciones
                    </Text>
                    <FlatList
                        data={this.state.recomendaciones}
                        keyExtractor={ (item, id) => item.id}
                        numColumns={2}
                        renderItem={({item})=>(
                            <TouchableHighlight>
                                <Card
                                    containerStyle={{
                                        width: width * 0.43,
                                        padding:0,
                                        marginLeft: 0,
                                        marginTop: 2
                                    }}
                                    image={{ uri: item.lugar.imagenes[0] }}>
                                    <Text style={{color: colors.primaryTextColor, fontSize: 19}}>
                                        {item.lugar.nombre_lugar}
                                    </Text>
                                    <Text style={{marginBottom: 10, color: colors.secondaryTextColor, fontSize: 14}}>
                                        <Icon name="md-pin" color={colors.primaryTextColor} size={14} /> {item.ciudad.nombre}
                                    </Text>
                                </Card>
                            </TouchableHighlight>
                        )}
                    />
                </View>
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        width, 
        height,
        backgroundColor: "#ffffff",
        paddingTop: 10,
        paddingLeft: lateralPadding,
        paddingRight: lateralPadding,
        paddingBottom: 10
    },
    tileBadgeStyle: { 
        height: 45, 
        width: 65, 
        borderRadius: 2.5, 
        backgroundColor: colors.accentColor ,
        flex: 1, 
        flexDirection: "column", 
        alignItems: "center",
        alignContent: "center" ,
        justifyContent: "center"
    }
})