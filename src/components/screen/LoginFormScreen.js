import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import {
    Input
} from 'react-native-elements';
import colors from '../../assets/pallete.js';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';

const { width, height } = Dimensions.get('window');
export default class LoginFormScreen extends React.Component {
    
    static navigationOptions = ({navigation}) => ({
        title: "Inicia sesión",
        headerStyle: {
            backgroundColor: colors.defaultPrimaryColor
        },
        headerTintColor: colors.textPrimaryColor,
        headerTitleContainerStyle: {
            width: "68%",
            justifyContent: "center",
            alignItems: "center",
            alignContent: "center"
        },
        headerTitleStyle: {
            fontFamily: colors.firstFontFamily
        }
    })

    constructor(props){
        super(props);
        this.state = {
            password: "",
            visiblePassword: false,
            email: ""
        }
        this._passwordInputRef = null;
    }

    _onVisiblePassword = () =>{
        this.setState({
            visiblePassword: !this.state.visiblePassword
        });
        this._passwordInputRef.setNativeProps({
            selection: {
                start: this.state.password.length, 
                end: this.state.password.length
            }
        });
    }

    _register = (data) => {
        firebase.auth().createUserWithEmailAndPassword( data.email, data.password)
        .then( register => {
            return register.user.sendEmailVerification();
        })
        .then( resp => {
            this.props.navigation.goBack();
        } )
        .catch( e => {
            if( e.code == "auth/email-already-in-use" )
                Alert.alert("Advertencia", "Este correo ya esta en uso, pruebe con otro");
            else
                Alert.alert("Error", JSON.stringify(e))
        })
    }

    _onRegisterButton = () => {
        let register = {
            email: this.state.email,
            password: this.state.password
        }
        firebase.auth().signInWithEmailAndPassword(register.email, register.password)
        .then( response => {
            this.props.navigation.navigate('user')
        } )
        .catch( e => {
            if( e.code == "auth/wrong-password" || e.code == "auth/user-not-found" )
                Alert.alert("Advertencia", "El usuario o la clave no coinciden, verifique y vuelva a intentar");
            else
                Alert.alert("ERROR", JSON.stringify(e));
        })
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <StatusBar backgroundColor={colors.darkPrimaryColor} />
                <View style={styles.mainContentStyle}>
                    <Input 
                        placeholder="Correo electronico"
                        inputContainerStyle={[styles.inputContainerStyle, {width: "100%"}]}
                        containerStyle={[styles.inputContentStyle, {width: "100%", marginTop: 18}]}
                        leftIcon={{ type: "ionicon", name: "md-mail", color: colors.secondaryTextColor }}
                        keyboardType="email-address"
                        onChangeText={ email => this.setState({ email }) }
                    />
                    <Input 
                        placeholder="Clave"
                        inputContainerStyle={[styles.inputContainerStyle, {width: "100%"}]}
                        containerStyle={[styles.inputContentStyle, {width: "100%", marginTop: 4}]}
                        leftIcon={{ type: "ionicon", name: "md-lock", color: colors.secondaryTextColor }}
                        secureTextEntry={ !this.state.visiblePassword }
                        ref={ ref => this._passwordInputRef = ref  }
                        onChangeText={password => this.setState({ password })}
                        rightIcon={
                            <TouchableOpacity onPress={()=> this.setState({ visiblePassword: !this.state.visiblePassword }) } style={{ marginRight: 10 }}>
                                <View>
                                    <Icon name={ !this.state.visiblePassword ? "md-eye": "md-eye-off" } color={colors.secondaryTextColor} size={30} />
                                </View>
                            </TouchableOpacity>
                        }
                    />
                    <View style={{ alignItems: "center", alignContent: "center", marginTop: 10 }}>
                        <TouchableOpacity onPress={()=> this._onRegisterButton() } style={{ width: "95%", height: 55, backgroundColor: colors.accentColor, alignItems: "center", alignContent: "center", borderRadius: 5, paddingTop: 10 }}>
                            <Text style={{ color: "#ffffff", fontFamily: colors.fontFamily, fontSize: 25, textAlign: "center" }} >
                               Entrar
                            </Text>
                        </TouchableOpacity>
                        <Text onPress={() => this.props.navigation.navigate('forgotPassword')} style={{ marginTop: 6,  textAlign: "center", color: colors.secondaryTextColor, fontSize: 24, fontFamily: colors.fontFamily }}>
                            ¿Olvidaste tu clave?
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        width,
        height,
        backgroundColor: "#f3f3f3"
    },
    mainContentStyle: {
        width,
        height,
        paddingTop: 10,
    },
    inputContainerStyle: {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0,
        height: 60
    },
    inputContentStyle:  {
        backgroundColor: "#ffffff",
        marginLeft: 0,
        width: "50%",
        height: 60
    }
})