import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import {
    Input
} from 'react-native-elements';
import colors from '../../assets/pallete.js';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';

const { width, height } = Dimensions.get('window');
export default class ForgotPassword extends React.Component {
    
    static navigationOptions = ({navigation}) => ({
        title: "Recuperar contraseña",
        headerStyle: {
            backgroundColor: colors.defaultPrimaryColor
        },
        headerTintColor: colors.textPrimaryColor,
        headerTitleContainerStyle: {
            width: "68%",
            justifyContent: "center",
            alignItems: "center",
            alignContent: "center"
        },
        headerTitleStyle: {
            fontFamily: colors.firstFontFamily
        }
    })

    constructor(props){
        super(props);
        this.state = {
            email: ""
        }
        this._passwordInputRef = null;
    }

    _onVisiblePassword = () =>{
        this.setState({
            visiblePassword: !this.state.visiblePassword
        });
        this._passwordInputRef.setNativeProps({
            selection: {
                start: this.state.password.length, 
                end: this.state.password.length
            }
        });
    }



    _onRegisterButton = () => {

        firebase.auth().sendPasswordResetEmail(this.state.email)
        .then( resp => {
            this.props.navigation.goBack();
        } )
        .catch( e=>{
            if( e.code == "auth/user-not-found" )
                Alert.alert("Advertencia", "El correo electronico no se encuentra registrado");
            else
                Alert.alert("ERROR", JSON.stringify(e))
        } )
    }

    render(){
        return(
            <View style={styles.mainContainer}>
                <StatusBar backgroundColor={colors.darkPrimaryColor} />
                <View style={styles.mainContentStyle}>
                    <Text style={{ color: colors.secondaryTextColor, fontFamily: colors.fontFamily, fontSize: 19, marginLeft: 4 }}>
                        Recupera tu contraseña
                    </Text>
                    <Input 
                        placeholder="Correo electronico"
                        inputContainerStyle={[styles.inputContainerStyle, {width: "100%"}]}
                        containerStyle={[styles.inputContentStyle, {width: "100%", marginTop: 18}]}
                        leftIcon={{ type: "ionicon", name: "md-mail", color: colors.secondaryTextColor }}
                        keyboardType="email-address"
                        onChangeText={ email => this.setState({ email }) }
                    />
                    <View style={{ alignItems: "center", alignContent: "center", marginTop: 10 }}>
                        <TouchableOpacity onPress={()=> this._onRegisterButton() } style={{ width: "95%", height: 55, backgroundColor: colors.accentColor, alignItems: "center", alignContent: "center", borderRadius: 5, paddingTop: 10 }}>
                            <Text style={{ color: "#ffffff", fontFamily: colors.fontFamily, fontSize: 25, textAlign: "center" }} >
                               Enviar
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        width,
        height,
        backgroundColor: "#f3f3f3"
    },
    mainContentStyle: {
        width,
        height,
        paddingTop: 10,
    },
    inputContainerStyle: {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0,
        height: 60
    },
    inputContentStyle:  {
        backgroundColor: "#ffffff",
        marginLeft: 0,
        width: "50%",
        height: 60
    }
})